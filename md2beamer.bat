@echo off

::
:: Calls pandoc's markdown -> LaTex conversion
::

setlocal
path "%PROGRAMFILES%\MiKTeX 2.9\miktex\bin\";C:\bin\pandoc;%PATH%

REM HACK: I added the line
REM   \setbeamercolor{item}{fg=darkred}
REM to ...\AppData\Roaming\MiKTeX\2.9\tex\latex\beamer\base\themes\color\beamercolorthemebeaver.sty

@echo on

pandoc -f markdown -t beamer -V colortheme:beaver -o %~n1.pdf %1

start ./%~n1.pdf
