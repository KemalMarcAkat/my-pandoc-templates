﻿@echo off

::
:: Calls pandoc's markdown -> DOCX conversion
::

setlocal
path C:\bin\pandoc;%PATH%

@echo on

%~d1
cd %~p1

if not exist *.csl goto md2docx
if not exist *.bib goto md2docx

REM Which CSL or BIB file to take if there is more than one?!

set CSL_FILE=
for %%S in (*.csl) do set CSL_FILE=%%S
set BIB_FILE=
for %%T in (*.bib) do set BIB_FILE=%%T

pandoc -f markdown -t docx --csl=%CSL_FILE% --bibliography=%BIB_FILE% -o %~n1.docx %1
goto showdocx

:md2docx
pandoc -f markdown -t docx -o %~n1.docx %1

:showdocx
start %~n1.docx
