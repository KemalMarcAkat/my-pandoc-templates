@echo off

::
:: Calls pandoc's markdown -> HTML conversion
::

setlocal
path C:\bin\pandoc;%PATH%

@echo on

%~d1
cd %~p1

:md2html
pandoc -f markdown -t html -s -o %~n1.html %1

:showhtml
start %~n1.html
