@echo off

::
:: Calls pandoc's markdown -> markdown conversion
:: (makes sense if one wants to resolve citations)
::

setlocal
path C:\bin\pandoc;%PATH%

@echo on

%~d1
cd %~p1

if not exist *.csl goto copymd
if not exist *.bib goto copymd

REM Which CSL or BIB file to take if there is more than one?!

set CSL_FILE=
for %%S in (*.csl) do set CSL_FILE=%%S
set BIB_FILE=
for %%T in (*.bib) do set BIB_FILE=%%T

:md2md
pandoc -f markdown -t markdown --template=md2md.template --csl=%CSL_FILE% --bibliography=%BIB_FILE% -o %~n1.md %1
goto showmd

:copymd
copy %1 %~n1.md

:showmd
start %~n1.md
