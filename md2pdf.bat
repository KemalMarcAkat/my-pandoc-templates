@echo off

::
:: Calls pandoc's markdown -> LaTex conversion
::

setlocal
path "%PROGRAMFILES%\MiKTeX 2.9\miktex\bin\";C:\bin\pandoc;%PATH%

@echo on

%~d1
cd %~p1

if exist makepdf.bat goto makepdf

if not exist *.csl goto md2tex
if not exist *.bib goto md2tex

REM Which CSL or BIB file to take if there is more than one?!

set CSL_FILE=
for %%S in (*.csl) do set CSL_FILE=%%S
set BIB_FILE=
for %%T in (*.bib) do set BIB_FILE=%%T

pandoc -f markdown -t latex --template=md2pdf.template --csl=%CSL_FILE% --bibliography=%BIB_FILE% -o %~n1.tex %1
goto tex2pdf

:makepdf
call makepdf.bat
goto showpdf

:md2tex
pandoc -f markdown -t latex --template=md2pdf.template -o %~n1.tex %1
goto tex2pdf

:tex2pdf
texify --pdf --quiet --clean --tex-option=-synctex=1 %~n1.tex

:showpdf
start %~n1.pdf
