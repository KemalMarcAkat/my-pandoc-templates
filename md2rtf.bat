@echo off

::
:: Calls pandoc's markdown -> RTF conversion
::

setlocal
path C:\bin\pandoc;%PATH%

@echo on

%~d1
cd %~p1

if not exist *.csl goto md2rtf
if not exist *.bib goto md2rtf

REM Which CSL or BIB file to take if there is more than one?!

set CSL_FILE=
for %%S in (*.csl) do set CSL_FILE=%%S
set BIB_FILE=
for %%T in (*.bib) do set BIB_FILE=%%T

pandoc -f markdown -t rtf --template=md2rtf.template --variable=links-as-notes --csl=%CSL_FILE% --bibliography=%BIB_FILE% -o %~n1.rtf %1
goto showrtf

:md2rtf
pandoc -f markdown -t rtf --template=md2rtf.template --variable=links-as-notes -o %~n1.rtf %1

:showrtf
start %~n1.rtf
