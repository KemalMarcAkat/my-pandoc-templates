@echo off

::
:: Calls pandoc's markdown -> LaTex conversion
::

setlocal
path "%PROGRAMFILES%\MiKTeX 2.9\miktex\bin\";C:\bin\pandoc;%PATH%

@echo on

::: Ok, I admit it: I removed the ugly Gill Sans entry in
::: <somepath>/cabal/pandoc-1.9.3/slidy/styles/slidy.css ...
pandoc -f markdown -t slidy --self-contained --data-dir=C:\bin\pandoc\data -o %~n1.html %1

start ./%~n1.html
